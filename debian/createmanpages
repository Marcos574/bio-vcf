#!/bin/sh

set -e

if [ ! -x /usr/bin/help2man ]; then
    echo "E: Missing /usr/bin/help2man, please install it from the cognate package."
    exit 1
fi

if [ ! -n "$NAME" ]; then
    NAME=`grep "^Description:" debian/control | sed 's/^Description: *//' | head -n1`
fi

if [ ! -n "$VERSION" ]; then
    VERSION=`dpkg-parsechangelog | awk '/^Version:/ {print $2}' | sed -e 's/^[0-9]*://' -e 's/-.*//' -e 's/[+~]dfsg$//'`
fi

if [ ! -n "$PROGNAME" ]; then
    PROGNAME=`grep "^Package:" debian/control | sed 's/^Package: *//' | head -n1`
fi

MANDIR=debian

HELPOPTION="--help"
echo "PROGNAME: '$PROGNAME'"
echo "NAME:     '$NAME'"
echo "VERSION:  '$VERSION'"
echo "MANDIR:   '$MANDIR'"
echo "HELPOPTION: '$HELPOPTION'"

mkdir -p $MANDIR


AUTHOR=".SH AUTHOR\n \
This manpage was written by $DEBFULLNAME for the Debian distribution and\n \
can be used for any other usage of the program.\
"

# If program name is different from package name or title should be
# different from package short description change this here
progname=${PROGNAME}
help2man --no-info --no-discard-stderr --help-option="$HELPOPTION" \
         --name="$NAME" \
            --version-string="$VERSION" ${progname} > $MANDIR/${progname}.1
echo $AUTHOR >> $MANDIR/${progname}.1

echo "$MANDIR/*.1" > debian/manpages

cat <<EOT
Please enhance the help2man output in '$MANDIR/${progname}.1'.
To inspect it, try 'nroff -man $MANDIR/${progname}.1'.
If very unhappy, try passing the HELPOPTION as an environment variable.
The following web page might be helpful in doing so:
    http://liw.fi/manpages/
EOT
